package hotel.helpers;

import hotel.credit.CreditCard;
import hotel.entities.*;

import java.util.Date;

public class BookingHelper {
    private static BookingHelper self;

    public BookingHelper() {}

    public static BookingHelper getInstance() {
        if(self == null) {
            self = new BookingHelper();
        }
        return self;
    }

    public Booking makeBooking(Guest g, Room r, Date d, int sl, int no, CreditCard c) {
        return new Booking(g, r, d, sl, no, c);
    }
}
