package hotel.booking;


import hotel.HotelHelper;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;



public class TestBookingScenario {

    @Mock BookingUI bookingUI;
    Hotel hotel;

    BookingCTL control;
    static SimpleDateFormat format;
    Date arrivalDate;




    @BeforeAll
    static void setUpBeforeClass() {
        format = new SimpleDateFormat("dd-MM-yyyy");


    }


    @AfterAll
    static void tearDownAfterClass() {
    }


    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);

        //leaving the line in from HotelHelper that will print out the room types to show that program starts.
        //would love to validate the user prompt messages, struggling to do so at this stage.
        hotel = HotelHelper.loadHotel();
        control = new BookingCTL(hotel);
        control.bookingUI = bookingUI;



    }


    @AfterEach
    void tearDown() {

    }

    @Test
    void testExistingGuestBooksAvailableRoom() throws ParseException {

        //arrange
        int phone = 2;
        RoomType roomType = RoomType.SINGLE;
        int occupantNumber = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("11-11-2011");
        int stayLength = 1;
        CreditCardType cardType = CreditCardType.VISA;
        int cardNum = 1;
        int ccv = 1;




        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> numCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> vendorCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> cardNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Long> confNumCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);


        //act
        control.phoneNumberEntered(phone);
        assertTrue(control.state == BookingCTL.State.ROOM);
        verify(bookingUI, times (1)).setState(any());
        control.roomTypeAndOccupantsEntered(roomType, occupantNumber);

        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (2)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        verify(bookingUI, times (3)).setState(any());
        control.creditDetailsEntered(cardType, cardNum, ccv);
        assertTrue(control.state == BookingCTL.State.COMPLETED);
        verify(bookingUI, times (4)).setState(any());
        control.completed();

        //assert
        verify(bookingUI).displayGuestDetails( nameCaptor.capture(), addressCaptor.capture(), phoneNumCaptor.capture());
        verify(bookingUI).displayBookingDetails(descCaptor.capture(), dateCaptor.capture(),stayCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).displayConfirmedBooking(
                descCaptor.capture(), numCaptor.capture(),
                dateCaptor.capture(), stayCaptor.capture(),
                nameCaptor.capture(), vendorCaptor.capture(),
                cardNumCaptor.capture(), costCaptor.capture(),
                confNumCaptor.capture());
        verify(bookingUI).displayMessage("Booking completed");

        /* assertEquals(expectedMainMenuMsg,
                "\nHotel Management System \n\n" +
                "Please select:\n\n" +
                "    B:    Book a Room\n" +
                "    C:    Check In\n" +
                "    R:    Record Service\n" +
                "    D:    Check Out\n" +
                " \n" +
                "    Q:   Quit\n" +
                " \n\n" +
                "Selection : ");
        assertEquals(expectedNameInputMsg,"Enter guest name: " );
        assertEquals(expectedAddressInputMsg,"Enter guest address: " );
        assertEquals(expectedArrivalDateInputMsg, "Enter arrival date");
        assertEquals(expectedCardNumberInputMsg, "Enter credit card number: ");
        assertEquals(expectedCCVInputMsg, "Enter CCV: ");
        assertEquals(expectedLengthInputMsg,"Enter length of stay: " );
        assertEquals(expectedPhoneInputMsg, "Enter phone number: ");
        assertEquals(expectedRoomTypeInputMsg,"Enter room type" );
        assertEquals(expectedOccupantsInputMsg,"Enter number of occupants: " );
        assertEquals(expectedArrivalDateInputMsg,"Enter arrival date" );
        */

        assertEquals(roomType.getDescription(), descCaptor.getValue());
        assertEquals(101, (int) numCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(stayLength, (int) stayCaptor.getValue());
        assertEquals("Fred", nameCaptor.getValue());
        assertEquals(cardType.getVendor(), vendorCaptor.getValue());
        assertEquals(cardNum, (int) cardNumCaptor.getValue());
        assertEquals("Nurke", addressCaptor.getValue());
        assertEquals(phone, (int) phoneNumCaptor.getValue());

        hotel.findBookingByConfirmationNumber(confNumCaptor.getValue());
    }


    @Test
    void testNewGuestBooksAvailableRoom() throws ParseException {

        //arrange
        int phone = 1;
        String name = "Josh";
        String address = "1 Fake St";
        RoomType roomType = RoomType.SINGLE;

        int occupantNumber = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("11-11-2011");
        int stayLength = 1;
        CreditCardType cardType = CreditCardType.VISA;
        int cardNum = 1;
        int ccv = 1;



        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> numCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> vendorCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> cardNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Long> confNumCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);


        //act
        control.phoneNumberEntered(phone);
        assertTrue(control.state == BookingCTL.State.REGISTER);
        verify(bookingUI, times (1)).setState(any());
        control.guestDetailsEntered(name, address);
        assertTrue(control.state == BookingCTL.State.ROOM);
        verify(bookingUI, times (2)).setState(any());
        control.roomTypeAndOccupantsEntered(roomType, occupantNumber);
        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (3)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        verify(bookingUI, times (4)).setState(any());
        control.creditDetailsEntered(cardType, cardNum, ccv);
        assertTrue(control.state == BookingCTL.State.COMPLETED);
        verify(bookingUI, times (5)).setState(any());
        control.completed();

        //assert
        verify(bookingUI).displayGuestDetails( nameCaptor.capture(), addressCaptor.capture(), phoneNumCaptor.capture());
        verify(bookingUI).displayBookingDetails(descCaptor.capture(), dateCaptor.capture(),stayCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).displayConfirmedBooking(
                descCaptor.capture(), numCaptor.capture(),
                dateCaptor.capture(), stayCaptor.capture(),
                nameCaptor.capture(), vendorCaptor.capture(),
                cardNumCaptor.capture(), costCaptor.capture(),
                confNumCaptor.capture());

       /* assertEquals(expectedMainMenuMsg,
                "\nHotel Management System \n\n" +
                        "Please select:\n\n" +
                        "    B:    Book a Room\n" +
                        "    C:    Check In\n" +
                        "    R:    Record Service\n" +
                        "    D:    Check Out\n" +
                        " \n" +
                        "    Q:   Quit\n" +
                        " \n\n" +
                        "Selection : ");
        verify(bookingUI).displayMessage("Booking completed");
        assertEquals(expectedNameInputMsg,"Enter guest name: " );
        assertEquals(expectedAddressInputMsg,"Enter guest address: " );
        assertEquals(expectedArrivalDateInputMsg, "Enter arrival date");
        assertEquals(expectedCardNumberInputMsg, "Enter credit card number: ");
        assertEquals(expectedCCVInputMsg, "Enter CCV: ");
        assertEquals(expectedLengthInputMsg,"Enter length of stay: " );
        assertEquals(expectedPhoneInputMsg, "Enter phone number: ");
        assertEquals(expectedRoomTypeInputMsg,"Enter room type" );
        assertEquals(expectedOccupantsInputMsg,"Enter number of occupants: " );
        assertEquals(expectedArrivalDateInputMsg,"Enter arrival date" );
        */


        assertEquals(roomType.getDescription(), descCaptor.getValue());
        assertEquals(101, (int) numCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(stayLength, (int) stayCaptor.getValue());
        assertEquals(name, nameCaptor.getValue());
        assertEquals(cardType.getVendor(), vendorCaptor.getValue());
        assertEquals(cardNum, (int) cardNumCaptor.getValue());
        assertEquals(address, addressCaptor.getValue());
        assertEquals(phone, (int) phoneNumCaptor.getValue());

        hotel.findBookingByConfirmationNumber(confNumCaptor.getValue());
    }


    @Test
    void testExistingGuestTryToBookUnAvailableRoomThenBooksAnotherRoomAvailable() throws ParseException {
        //arrange
        int phone = 2;
        RoomType roomType1 = RoomType.TWIN_SHARE;
        RoomType roomType2 = RoomType.DOUBLE;
        int occupantNumber = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("01-01-0001");
        int stayLength = 1;
        CreditCardType cardType = CreditCardType.VISA;
        int cardNum = 1;
        int ccv = 1;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(arrivalDate);
        calendar.add(Calendar.DATE, stayLength);
        Date departureDate = calendar.getTime();


        String notAvailableStr = String.format("\n%s is not available between %s and %s\n",
                roomType1.getDescription(),
                format.format(arrivalDate),
                format.format(departureDate));


        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> numCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> vendorCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> cardNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Long> confNumCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);


        //act
        control.phoneNumberEntered(phone);
        assertTrue(control.state == BookingCTL.State.ROOM);
        verify(bookingUI, times (1)).setState(any());
        control.roomTypeAndOccupantsEntered(roomType1, occupantNumber);
        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (2)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        //hard set state back to room to change room - else need to create a way to start
        //from the start. UC for dot point No Room Available should go to step 10 - however
        //Key Scenarios states that Existing Guest attempts to book an unavailable room ,
        // then books another type of room
        control.state = BookingCTL.State.ROOM;
        assertTrue(control.state == BookingCTL.State.ROOM);
        control.roomTypeAndOccupantsEntered(roomType2, occupantNumber);
        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (3)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        verify(bookingUI, times (4)).setState(any());
        control.creditDetailsEntered(cardType, cardNum, ccv);
        assertTrue(control.state == BookingCTL.State.COMPLETED);
        verify(bookingUI, times (5)).setState(any());
        control.completed();

        //assert
        verify(bookingUI).displayGuestDetails( nameCaptor.capture(), addressCaptor.capture(), phoneNumCaptor.capture());
        verify(bookingUI).displayBookingDetails(descCaptor.capture(), dateCaptor.capture(),stayCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).displayMessage(notAvailableStr);
        verify(bookingUI).displayConfirmedBooking(
                descCaptor.capture(), numCaptor.capture(),
                dateCaptor.capture(), stayCaptor.capture(),
                nameCaptor.capture(), vendorCaptor.capture(),
                cardNumCaptor.capture(), costCaptor.capture(),
                confNumCaptor.capture());

      /*  assertEquals(expectedMainMenuMsg,
                "\nHotel Management System \n\n" +
                        "Please select:\n\n" +
                        "    B:    Book a Room\n" +
                        "    C:    Check In\n" +
                        "    R:    Record Service\n" +
                        "    D:    Check Out\n" +
                        " \n" +
                        "    Q:   Quit\n" +
                        " \n\n" +
                        "Selection : ");

        verify(bookingUI).displayMessage("Booking completed");
        assertEquals(expectedNameInputMsg,"Enter guest name: " );
        assertEquals(expectedAddressInputMsg,"Enter guest address: " );
        assertEquals(expectedArrivalDateInputMsg, "Enter arrival date");
        assertEquals(expectedCardNumberInputMsg, "Enter credit card number: ");
        assertEquals(expectedCCVInputMsg, "Enter CCV: ");
        assertEquals(expectedLengthInputMsg,"Enter length of stay: " );
        assertEquals(expectedPhoneInputMsg, "Enter phone number: ");
        assertEquals(expectedRoomTypeInputMsg,"Enter room type" );
        assertEquals(expectedOccupantsInputMsg,"Enter number of occupants: " );
        assertEquals(expectedArrivalDateInputMsg,"Enter arrival date" );

        */

        assertEquals(roomType2.getDescription(), descCaptor.getValue());
        assertEquals(201, (int) numCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(stayLength, (int) stayCaptor.getValue());
        assertEquals("Fred", nameCaptor.getValue());
        assertEquals(cardType.getVendor(), vendorCaptor.getValue());
        assertEquals(cardNum, (int) cardNumCaptor.getValue());
        assertEquals("Nurke", addressCaptor.getValue());
        assertEquals(phone, (int) phoneNumCaptor.getValue());

    }

    @Test
    void testExistingGuestAttemptsBookingAndCancels() throws ParseException {

        //arrange
        int phone = 2;
        RoomType roomType1 = RoomType.TWIN_SHARE;
        int occupantNumber = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("01-01-0001");
        int stayLength = 1;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(arrivalDate);
        calendar.add(Calendar.DATE, stayLength);
        Date departureDate = calendar.getTime();


        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneNumCaptor = ArgumentCaptor.forClass(Integer.class);

        String notAvailableStr = String.format("\n%s is not available between %s and %s\n",
                roomType1.getDescription(),
                format.format(arrivalDate),
                format.format(departureDate));


        //act
        control.phoneNumberEntered(phone);
        assertTrue(control.state == BookingCTL.State.ROOM);
        verify(bookingUI, times (1)).setState(any());
        control.roomTypeAndOccupantsEntered(roomType1, occupantNumber);
        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (2)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        assertTrue(control.state == BookingCTL.State.TIMES);
        control.cancel();
        assertTrue(control.state == BookingCTL.State.CANCELLED);
        verify(bookingUI, times (3)).setState(any());


        //assert
        verify(bookingUI).displayMessage(notAvailableStr);

      /*  assertEquals(expectedMainMenuMsg,
                "\nHotel Management System \n\n" +
                        "Please select:\n\n" +
                        "    B:    Book a Room\n" +
                        "    C:    Check In\n" +
                        "    R:    Record Service\n" +
                        "    D:    Check Out\n" +
                        " \n" +
                        "    Q:   Quit\n" +
                        " \n\n" +
                        "Selection : ");

        verify(bookingUI).displayMessage("Booking cancelled");
        assertEquals(expectedNameInputMsg,"Enter guest name: " );
        assertEquals(expectedAddressInputMsg,"Enter guest address: " );
        assertEquals(expectedArrivalDateInputMsg, "Enter arrival date");
        assertEquals(expectedCardNumberInputMsg, "Enter credit card number: ");
        assertEquals(expectedCCVInputMsg, "Enter CCV: ");
        assertEquals(expectedLengthInputMsg,"Enter length of stay: " );
        assertEquals(expectedPhoneInputMsg, "Enter phone number: ");
        assertEquals(expectedRoomTypeInputMsg,"Enter room type" );
        assertEquals(expectedOccupantsInputMsg,"Enter number of occupants: " );
        assertEquals(expectedArrivalDateInputMsg,"Enter arrival date" );
        */


        verify(bookingUI).displayGuestDetails( nameCaptor.capture(), addressCaptor.capture(), phoneNumCaptor.capture());
        //Following diagram and code, shows that display for bookingdetails only happens if successful
    }


    @Test
    void testExistingGuestBadCreditThenGoodCredit() throws ParseException {

        //arrange
        int phone = 2;
        RoomType roomType = RoomType.SINGLE;
        int occupantNumber = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("11-11-2011");
        int stayLength = 1;
        CreditCardType cardType = CreditCardType.VISA;
        int cardNum1 = 7;
        int cardNum2 = 1;
        int ccv = 1;

        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> numCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> vendorCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> cardNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Long> confNumCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);

        String notAvailableStr = String.format("Credit Card not approved");

        //act
        control.phoneNumberEntered(phone);
        assertTrue(control.state == BookingCTL.State.ROOM);
        verify(bookingUI, times (1)).setState(any());
        control.roomTypeAndOccupantsEntered(roomType, occupantNumber);
        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (2)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        verify(bookingUI, times (3)).setState(any());
        control.creditDetailsEntered(cardType, cardNum1, ccv);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        control.creditDetailsEntered(cardType, cardNum2, ccv);
        verify(bookingUI, times (4)).setState(any());
        control.completed();

        //assert
        verify(bookingUI).displayGuestDetails( nameCaptor.capture(), addressCaptor.capture(), phoneNumCaptor.capture());
        verify(bookingUI).displayBookingDetails(descCaptor.capture(), dateCaptor.capture(),stayCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).displayConfirmedBooking(
                descCaptor.capture(), numCaptor.capture(),
                dateCaptor.capture(), stayCaptor.capture(),
                nameCaptor.capture(), vendorCaptor.capture(),
                cardNumCaptor.capture(), costCaptor.capture(),
                confNumCaptor.capture());
        verify(bookingUI).displayMessage(notAvailableStr);

    /*    assertEquals(expectedMainMenuMsg,
                "\nHotel Management System \n\n" +
                        "Please select:\n\n" +
                        "    B:    Book a Room\n" +
                        "    C:    Check In\n" +
                        "    R:    Record Service\n" +
                        "    D:    Check Out\n" +
                        " \n" +
                        "    Q:   Quit\n" +
                        " \n\n" +
                        "Selection : ");

        verify(bookingUI).displayMessage("Booking completed");
        assertEquals(expectedNameInputMsg,"Enter guest name: " );
        assertEquals(expectedAddressInputMsg,"Enter guest address: " );
        assertEquals(expectedArrivalDateInputMsg, "Enter arrival date");
        assertEquals(expectedCardNumberInputMsg, "Enter credit card number: ");
        assertEquals(expectedCCVInputMsg, "Enter CCV: ");
        assertEquals(expectedLengthInputMsg,"Enter length of stay: " );
        assertEquals(expectedPhoneInputMsg, "Enter phone number: ");
        assertEquals(expectedRoomTypeInputMsg,"Enter room type" );
        assertEquals(expectedOccupantsInputMsg,"Enter number of occupants: " );
        assertEquals(expectedArrivalDateInputMsg,"Enter arrival date" );

        */

        assertEquals(roomType.getDescription(), descCaptor.getValue());
        assertEquals(101, (int) numCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(stayLength, (int) stayCaptor.getValue());
        assertEquals("Fred", nameCaptor.getValue());
        assertEquals(cardType.getVendor(), vendorCaptor.getValue());
        assertEquals(cardNum2, (int) cardNumCaptor.getValue());
        assertEquals("Nurke", addressCaptor.getValue());
        assertEquals(phone, (int) phoneNumCaptor.getValue());

        hotel.findBookingByConfirmationNumber(confNumCaptor.getValue());
    }


    @Test
    void testExistingGuestBadCreditThenCancels() throws ParseException {

        //arrange
        int phone = 2;
        RoomType roomType = RoomType.SINGLE;
        int occupantNumber = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("11-11-2011");
        int stayLength = 1;
        CreditCardType cardType = CreditCardType.VISA;
        int cardNum1 = 7;
        int ccv = 1;

        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);

        String notAvailableStr = String.format("Credit Card not approved");

        //act
        control.phoneNumberEntered(phone);
        assertTrue(control.state == BookingCTL.State.ROOM);
        verify(bookingUI, times (1)).setState(any());
        control.roomTypeAndOccupantsEntered(roomType, occupantNumber);
        assertTrue(control.state == BookingCTL.State.TIMES);
        verify(bookingUI, times (2)).setState(any());
        control.bookingTimesEntered(arrivalDate, stayLength);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        verify(bookingUI, times (3)).setState(any());
        control.creditDetailsEntered(cardType, cardNum1, ccv);
        assertTrue(control.state == BookingCTL.State.CREDIT);
        control.cancel();
        assertTrue(control.state == BookingCTL.State.CANCELLED);
        verify(bookingUI, times (4)).setState(any());

        //assert
        verify(bookingUI).displayGuestDetails( nameCaptor.capture(), addressCaptor.capture(), phoneNumCaptor.capture());
        verify(bookingUI).displayBookingDetails(descCaptor.capture(), dateCaptor.capture(),stayCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).displayMessage(notAvailableStr);

     /*   assertEquals(expectedMainMenuMsg,
                "\nHotel Management System \n\n" +
                        "Please select:\n\n" +
                        "    B:    Book a Room\n" +
                        "    C:    Check In\n" +
                        "    R:    Record Service\n" +
                        "    D:    Check Out\n" +
                        " \n" +
                        "    Q:   Quit\n" +
                        " \n\n" +
                        "Selection : ");

        verify(bookingUI).displayMessage("Booking cancelled");
        assertEquals(expectedNameInputMsg,"Enter guest name: " );
        assertEquals(expectedAddressInputMsg,"Enter guest address: " );
        assertEquals(expectedArrivalDateInputMsg, "Enter arrival date");
        assertEquals(expectedCardNumberInputMsg, "Enter credit card number: ");
        assertEquals(expectedCCVInputMsg, "Enter CCV: ");
        assertEquals(expectedLengthInputMsg,"Enter length of stay: " );
        assertEquals(expectedPhoneInputMsg, "Enter phone number: ");
        assertEquals(expectedRoomTypeInputMsg,"Enter room type" );
        assertEquals(expectedOccupantsInputMsg,"Enter number of occupants: " );
        assertEquals(expectedArrivalDateInputMsg,"Enter arrival date" );

        */

        assertEquals(roomType.getDescription(), descCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(stayLength, (int) stayCaptor.getValue());
        assertEquals("Fred", nameCaptor.getValue());
        assertEquals("Nurke", addressCaptor.getValue());
        assertEquals(phone, (int) phoneNumCaptor.getValue());

    }


}

