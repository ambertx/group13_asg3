package hotel.booking;

import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.helpers.CreditCardHelper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class TestBookingCTLIntegration {


    @Mock CreditAuthorizer authorizer;
    @Mock CreditCardHelper creditCardHelper;


    @Mock BookingUI bookingUI;
    @Mock Room room;
    @Mock Guest guest;
    @Mock Hotel hotel;
    BookingCTL control;
    RoomType roomType;

    int occupantNumber;
    Date arrivalDate;
    int stayLength;
    int cardNum;
    CreditCardType cardType;
    long confNum;
    private double cost;
    static SimpleDateFormat format;
    int ccv;
    String roomDescription;


    ArgumentCaptor<BookingUI.State> uiStateCaptor = ArgumentCaptor.forClass(BookingUI.State.class);

    @BeforeAll
    static void setUpBeforeClass() {
        format = new SimpleDateFormat("dd-MM-yyyy");
    }


    @AfterAll
    static void tearDownAfterClass() {
    }


    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        control = new BookingCTL(hotel);

        control.bookingUI = bookingUI;
        control.authorizer = authorizer;
        control.creditCardHelper = creditCardHelper;


        arrivalDate = format.parse("11-11-2011");
        stayLength = 1;
        occupantNumber = 1;
        cardNum = 1;
        cardType = CreditCardType.VISA;
        cost = 111.11;
        confNum = 11112011101L;
        ccv = 1;
        roomType = RoomType.SINGLE;
    }


    @AfterEach
    void tearDown() {

    }

    @Test
    void testCreditCardDetailsEntered_RealCreditCardCreditNotApproved() {

        //arrange
        control.state = BookingCTL.State.CREDIT;

        creditCardHelper = new CreditCardHelper();
        control.creditCardHelper = creditCardHelper;
        authorizer = CreditAuthorizer.getInstance();
        control.authorizer = authorizer;
        cardNum = 7;
        String expectedMessage = String.format("Credit Card not approved");
        ArgumentCaptor<String> errorCaptor = ArgumentCaptor.forClass(String.class);

        assertTrue(control.state == BookingCTL.State.CREDIT);


        //act

        control.creditDetailsEntered(cardType,cardNum,ccv);

        //assert
        assertSame(control.state, BookingCTL.State.CREDIT);
        verify(bookingUI).displayMessage(errorCaptor.capture());
        assertEquals(expectedMessage, errorCaptor.getValue());


    }

    @Test
    void testCreditCardDetailsEntered_AllRealApproved(){

        //arrange
        control.state = BookingCTL.State.CREDIT;
        control.cost = cost;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.occupantNumber = occupantNumber;

        CreditCardType cardType = CreditCardType.VISA;
        control.creditCardHelper = new CreditCardHelper();
        control.authorizer = CreditAuthorizer.getInstance();

        hotel = new Hotel();
        control.hotel = hotel;

        String name = "Josh";
        String address = "1 fake st";
        int phoneNumber = 1;
        guest = new Guest(name,address,phoneNumber);
        control.guest = guest;

        int roomNumber = 101;
        RoomType roomType = RoomType.SINGLE;
        room = new Room(roomNumber, roomType);
        control.room = room;


        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> numCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> vendorCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> cardNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Long> confNumCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<BookingUI.State> uiStateCaptor = ArgumentCaptor.forClass(BookingUI.State.class);

        assertSame(control.state, BookingCTL.State.CREDIT);

        //act
        control.creditDetailsEntered(cardType, cardNum, ccv);

        //assert

        verify(bookingUI).displayConfirmedBooking(
                descCaptor.capture(), numCaptor.capture(),
                dateCaptor.capture(), stayCaptor.capture(),
                nameCaptor.capture(), vendorCaptor.capture(),
                cardNumCaptor.capture(), costCaptor.capture(),
                confNumCaptor.capture());

        verify(bookingUI).setState(uiStateCaptor.capture());


        assertEquals(roomType.getDescription(), descCaptor.getValue());
        assertEquals(room.getId(), (int) numCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(stayLength, (int) stayCaptor.getValue());
        assertEquals(name, nameCaptor.getValue());
        assertEquals(cardType.getVendor(), vendorCaptor.getValue());
        assertEquals(cardNum, (int) cardNumCaptor.getValue());
        assertEquals(cost, costCaptor.getValue(), 0.001);
        assertEquals(confNum, (long) confNumCaptor.getValue());

        assertTrue(BookingUI.State.COMPLETED == uiStateCaptor.getValue());
        assertTrue(BookingCTL.State.COMPLETED == control.state);
        control.completed();
        verify(bookingUI).displayMessage("Booking completed");

    }

    @Test
    void testBookingTimesEntered_RealRoomRoomAvailable(){
        //arrange
        control.state = BookingCTL.State.TIMES;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.guest = guest;
        control.occupantNumber = occupantNumber;
        control.selectedRoomType = roomType;
        RoomType roomType = RoomType.SINGLE;
        int roomNumber = 101;
        room = new Room(roomNumber, roomType);



        assertSame(control.state, BookingCTL.State.TIMES);

        when(hotel.findAvailableRoom(roomType, arrivalDate, stayLength)).thenReturn(room);

        ArgumentCaptor<String> roomDescriptionCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Date> arrivalDateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<Integer> stayLengthCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);

        //act
        control.bookingTimesEntered(arrivalDate, stayLength);

        // assert
        verify(bookingUI).displayBookingDetails(roomDescriptionCaptor.capture(),
                arrivalDateCaptor.capture(), stayLengthCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).setState(uiStateCaptor.capture());

        assertEquals("Single room", roomDescriptionCaptor.getValue());
        assertEquals(arrivalDate, arrivalDateCaptor.getValue());
        assertEquals(1, (int) stayLengthCaptor.getValue());
        assertSame(BookingUI.State.CREDIT, uiStateCaptor.getValue());
        assertSame(BookingCTL.State.CREDIT, control.state);
    }

    @Test
    void testBookingTimesEntered_RealRoom_RealGuestRoomAvailable(){
        //arrange
        control.state = BookingCTL.State.TIMES;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;

        String name = "Josh";
        String address = "1 fake st";
        int phoneNumber = 1;
        guest = new Guest(name,address,phoneNumber);
        control.guest = guest;
        control.occupantNumber = occupantNumber;

        roomDescription = roomType.getDescription();
        control.selectedRoomType = roomType;

        RoomType roomType = RoomType.SINGLE;
        int roomNumber = 101;
        room = new Room(roomNumber, roomType);


        assertSame(control.state, BookingCTL.State.TIMES);

        when(hotel.findAvailableRoom(roomType, arrivalDate, stayLength)).thenReturn(room);

        ArgumentCaptor<String> roomDescriptionCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Date> arrivalDateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<Integer> stayLengthCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);

        //act
        control.bookingTimesEntered(arrivalDate, stayLength);

        // assert
        verify(bookingUI).displayBookingDetails(roomDescriptionCaptor.capture(),
                arrivalDateCaptor.capture(), stayLengthCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).setState(uiStateCaptor.capture());


        assertEquals("Single room", roomDescriptionCaptor.getValue());
        assertEquals(arrivalDate, arrivalDateCaptor.getValue());
        assertEquals(1, (int) stayLengthCaptor.getValue());
        assertSame(BookingUI.State.CREDIT, uiStateCaptor.getValue());
        assertSame(BookingCTL.State.CREDIT, control.state);
    }

    @Test
    void testPhoneNumberEntered_RealNumberCorrect_RealGuestExists(){

        String name = "Josh";
        String address = "1 fake st";
        int phoneNumber = 1;
        guest = new Guest(name,address,phoneNumber);
        control.guest = guest;
        control.occupantNumber = occupantNumber;

        //arrange
        assertSame(control.state, BookingCTL.State.PHONE);
        when(hotel.isRegistered(anyInt())).thenReturn(true);
        when(hotel.findGuestByPhoneNumber(anyInt())).thenReturn(guest);



        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneCaptor = ArgumentCaptor.forClass(Integer.class);

        //act
        control.phoneNumberEntered(phoneNumber);

        //assert
        verify(hotel).isRegistered(anyInt());
        verify(hotel).findGuestByPhoneNumber(anyInt());
        verify(bookingUI).displayGuestDetails(nameCaptor.capture(), addressCaptor.capture(), phoneCaptor.capture());
        verify(bookingUI).setState(uiStateCaptor.capture());

        assertEquals(name, nameCaptor.getValue());
        assertEquals(address, addressCaptor.getValue());
        assertEquals(phoneNumber, (int) phoneCaptor.getValue());
        assertSame(BookingUI.State.ROOM, uiStateCaptor.getValue());
        assertSame(BookingCTL.State.ROOM, control.state);
    }



    @Test
    void testBookingTimesEntered_RealRoomRoomNotAvailable(){
        //arrange
        control.state = BookingCTL.State.TIMES;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.guest = guest;
        control.occupantNumber = occupantNumber;
        roomDescription = roomType.getDescription();
        control.selectedRoomType = roomType;
        RoomType roomType = RoomType.SINGLE;
        int roomNumber = 101;
        room = new Room(roomNumber, roomType);




        Calendar calendar = Calendar.getInstance();
        calendar.setTime(arrivalDate);
        calendar.add(Calendar.DATE, stayLength);
        Date departureDate = calendar.getTime();
        String formatDepartureDate = format.format(departureDate);
        String formatArrivalDate = format.format(arrivalDate);

        assertSame(control.state, BookingCTL.State.TIMES);

        hotel.findAvailableRoom(roomType, arrivalDate, stayLength);

        //act
        control.bookingTimesEntered(arrivalDate, stayLength);

        // assert
        verify(bookingUI).displayMessage("\n" + roomDescription + " is not available between " +
                formatArrivalDate + " and " + formatDepartureDate + "\n");
        assertSame(BookingCTL.State.TIMES, control.state);
    }

    @Test
    void testPhoneNumberEtered_RealPhoneCorrect_NotRegistered(){
        // arrange
        control.state = BookingCTL.State.PHONE;

        int phoneNumber = 1;

        //act
        control.phoneNumberEntered(phoneNumber);
        verify(bookingUI).setState(uiStateCaptor.capture());

        //assert
        assertSame(control.state, BookingCTL.State.REGISTER);
        assertSame(uiStateCaptor.getValue(), BookingUI.State.REGISTER);
    }



}







