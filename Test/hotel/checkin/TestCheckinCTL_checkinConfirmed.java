package hotel.checkin;

import hotel.entities.Booking;
import hotel.entities.Hotel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;


@ExtendWith(MockitoExtension.class)

/*
    Unit testing for TestCheckinCTL
 */

class TestCheckinCTL_checkinConfirmed {
    @Mock Hotel mockHotel;
    @Mock CheckinUI mockUI;
    @Mock Booking mockBooking;
    long confirmationNumber;
    CheckinCTL checkinController;

    ArgumentCaptor<String> uiMessageArgCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<CheckinUI.State> uiStateCaptor = ArgumentCaptor.forClass(CheckinUI.State.class);


    boolean confirmedCheckin;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        checkinController =  new CheckinCTL(mockHotel);
        confirmationNumber = 18092018101L;

        checkinController.confirmationNumber = confirmationNumber;
        checkinController.checkInUI = mockUI;
        checkinController.booking = mockBooking;

    }


    @AfterEach
    void tearDown() throws Exception {

    }

    @Test
    void TestCheckinConfirmed_WrongControllerState() {
        // arrange
        confirmedCheckin = true;
        String expectedExceptionMsg = "CheckInCTL: checkInConfirmed : bad state : CHECKING";

        // act
        Executable e = () -> checkinController.checkInConfirmed(confirmedCheckin);

        // assert
        Throwable t = assertThrows(RuntimeException.class, e);
        verifyZeroInteractions(mockHotel);
        assertEquals(expectedExceptionMsg, t.getMessage());
    }

    @Test
    void TestCheckinConfirmed_CorrectControllerState_CheckinConfirmedFalse() {
        // arrange
        confirmedCheckin = false;
        checkinController.state = CheckinCTL.State.CONFIRMING;

        // act
        checkinController.checkInConfirmed(confirmedCheckin);


        // assert
        verify(mockUI).displayMessage(uiMessageArgCaptor.capture());
        verify(mockUI).setState(uiStateCaptor.capture());
        verifyZeroInteractions(mockHotel);

        assertEquals("Check In cancelled.", uiMessageArgCaptor.getValue());
        assertTrue(checkinController.state == CheckinCTL.State.CANCELLED);
        assertTrue(uiStateCaptor.getValue() == CheckinUI.State.CANCELLED);
    }

    @Test
    void TestCheckinConfirmed_CorrectControllerState_ConfirmedTrue() {
        // arrange
        confirmedCheckin = true;
        checkinController.state = CheckinCTL.State.CONFIRMING;
        String expectedUiMessage = String.format("Check In of booking %s is now complete.", confirmationNumber);

        // act
        checkinController.checkInConfirmed(confirmedCheckin);

        // assert
        verify(mockHotel).checkin(confirmationNumber);
        verify(mockUI).displayMessage(uiMessageArgCaptor.capture());
        verify(mockUI).setState(uiStateCaptor.capture());

        assertEquals(expectedUiMessage, uiMessageArgCaptor.getValue());
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);
        assertTrue(uiStateCaptor.getValue() == CheckinUI.State.COMPLETED);
    }
/*class TestCheckinCTL_checkinConfirmed {
    Hotel hotel;
    Booking booking;
    Room room;
    CheckinCTL checkinController;

    @Mock Guest guest;
    @Mock CreditCard card;
    @Mock CheckinUI checkInUI;
    long confNum;

    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    Date date;

    ArgumentCaptor<String> uiMessageArgCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<CheckinUI.State> uiStateCaptor = ArgumentCaptor.forClass(CheckinUI.State.class);


    boolean confirmedCheckin;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        date = format.parse("01-01-0001");

        hotel = new Hotel();
        room = new Room(101, RoomType.SINGLE);

        confNum = hotel.book(room, guest, date,1, 1, card);

        checkinController =  new CheckinCTL(hotel);
        checkinController.confirmationNumberEntered(confNum);
        checkinController.checkInUI = checkInUI;

    }


    @AfterEach
    void tearDown() throws Exception {

    }

    @Test
    void testCheckinConfirmed_ConfirmedTrue_CorrectStates() {
        // arrange
        boolean confirmed = true;
        String expectedUiMessage = String.format("Check In of booking %s is now complete.", checkinController.confirmationNumber);
        assertTrue(checkinController.state == CheckinCTL.State.CONFIRMING);

        // act
        checkinController.checkInConfirmed(confirmed);
        verify(checkInUI).displayMessage(uiMessageArgCaptor.capture());
        verify(checkInUI).setState(uiStateCaptor.capture());

        // assert
        assertEquals(expectedUiMessage, uiMessageArgCaptor.getValue());
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);
        assertTrue(uiStateCaptor.getValue() == CheckinUI.State.COMPLETED);
        assertTrue(hotel.activeBookingsByRoomId.size() == 1);





    }*/
}
