package hotel.checkin;

import hotel.credit.CreditCard;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)

/*
    High-level Entity-Control integration testing
 */
class CheckinCTLIntegraton {
    @Mock Hotel hotel;
    @Mock CheckinUI checkInUI;
    @Mock Booking mockBooking;
    @Mock Room mockRoom;
    @Mock Guest mockGuest;
    @Mock CreditCard mockCard;

    SimpleDateFormat format;
    Date date;
    long confirmationNumber;
    CheckinCTL checkinController;

    ArgumentCaptor<String> uiMessageArgCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<CheckinUI.State> uiStateCaptor = ArgumentCaptor.forClass(CheckinUI.State.class);


    boolean confirmedCheckin;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        checkinController =  new CheckinCTL(hotel);
        confirmationNumber = 18092018101L;
        checkinController.confirmationNumber = confirmationNumber;
        checkinController.booking = mockBooking;
        checkinController.checkInUI = checkInUI;

        format = new SimpleDateFormat("dd-MM-yyyy");
        date = format.parse("13-10-2018");


    }


    @AfterEach
    void tearDown() throws Exception {

    }

    @Test
    void TestCheckinConfirmed_RealHotel_MockBooking() {
        // arrange
        checkinController.hotel = new Hotel();
        checkinController.hotel.bookingsByConfirmationNumber.put(confirmationNumber, mockBooking);

        boolean confirmed = true;
        checkinController.state = CheckinCTL.State.CONFIRMING;
        String expectedUiMessage = String.format("Check In of booking %s is now complete.", confirmationNumber);

        // act
        checkinController.checkInConfirmed(true);
        verify(checkInUI).displayMessage(uiMessageArgCaptor.capture());
        verify(checkInUI).setState(uiStateCaptor.capture());

        // assert
        verify(mockBooking).checkIn();
        assertEquals(expectedUiMessage, uiMessageArgCaptor.getValue());
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);
        assertTrue(uiStateCaptor.getValue() == CheckinUI.State.COMPLETED);
    }

    @Test
    void TestCheckinConfirmed_RealHotel_RealBooking() {
        // arrange
        checkinController.hotel = new Hotel();
        Booking booking = new Booking(mockGuest, mockRoom, date, 1, 1, mockCard);
        checkinController.booking = booking;
        checkinController.hotel.bookingsByConfirmationNumber.put(confirmationNumber, booking);

        boolean confirmed = true;
        checkinController.state = CheckinCTL.State.CONFIRMING;
        String expectedUiMessage = String.format("Check In of booking %s is now complete.", confirmationNumber);

        // act
        checkinController.checkInConfirmed(true);
        verify(checkInUI).displayMessage(uiMessageArgCaptor.capture());
        verify(checkInUI).setState(uiStateCaptor.capture());

        // assert
        assertTrue(checkinController.booking.isCheckedIn());
        assertEquals(expectedUiMessage, uiMessageArgCaptor.getValue());
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);
        assertTrue(uiStateCaptor.getValue() == CheckinUI.State.COMPLETED);
    }

    @Test
    void TestCheckinConfirmed_RealHotel_RealBookingWithRealRoom() {
        // arrange
        checkinController.hotel = new Hotel();
        Room room =  new Room(101, RoomType.SINGLE);
        Booking booking = new Booking(mockGuest, room, date, 1, 1, mockCard);
        checkinController.booking = booking;
        checkinController.hotel.bookingsByConfirmationNumber.put(confirmationNumber, booking);

        boolean confirmed = true;
        checkinController.state = CheckinCTL.State.CONFIRMING;
        String expectedUiMessage = String.format("Check In of booking %s is now complete.", confirmationNumber);

        // act
        checkinController.checkInConfirmed(true);
        verify(checkInUI).displayMessage(uiMessageArgCaptor.capture());
        verify(checkInUI).setState(uiStateCaptor.capture());

        // assert
        assertTrue(checkinController.booking.isCheckedIn());
        assertTrue(checkinController.booking.getRoom().isOccupied());
        assertEquals(expectedUiMessage, uiMessageArgCaptor.getValue());
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);
        assertTrue(uiStateCaptor.getValue() == CheckinUI.State.COMPLETED);
    }
}