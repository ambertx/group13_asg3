package hotel.entities;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.helpers.CreditCardHelper;
import hotel.helpers.GuestHelper;
import hotel.helpers.RoomHelper;
import hotel.helpers.ServiceChargeHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


/*
Scott Hayward.

At this point we have full implementations of all Entity classes, and each of them has presumably been unit tested.
Now it's time to see if they work correctly together.

The following tests will test how well the Hotel class integrates with the other entity classes(ie. with real
objects rather than Mocks), with respect to the book(), checkin(), checkout() and addServiceCharge() methods.

 */
@ExtendWith(MockitoExtension.class)
class TestHotelIntegration {
    int numOccupants;
    int stayLength;
    long confNumber;
    int mockRoomId;
    Date date;
    SimpleDateFormat format;

    Hotel hotel;
    Room room;
    Booking booking;
    @Mock Guest guest;
    @Mock CreditCard card;

    double mockServiceChargeCost;


    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        format = new SimpleDateFormat("dd-MM-yyyy");
        date = format.parse("13-10-2018");
        mockRoomId = 101;
        numOccupants = 1;
        stayLength = 2;
        mockServiceChargeCost = 100.00;

        hotel = new Hotel();
        room = new Room(mockRoomId, RoomType.SINGLE);

        // We need a Booking to exist before each test
        confNumber = hotel.book(room, guest, date, stayLength, numOccupants, card);
    }


    @AfterEach
    void tearDown() throws Exception {

    }

    /* This test uses real Room, and mocked Guest and CreditCard objects to create a new Booking.
       After calling the hotel.book() method
            A booking should exist for the room
            The room should not be available for the specified arrivalDate and staylength
            The booking should be returned from findBookingByConfirmationNumber()
            The Booking state should be PENDING.
     */
    @Test
    void TestBookMethod_Hotel_Room_Booking_Integration() {
        // arrange
        // act
        assertNotNull(hotel.findBookingByConfirmationNumber(confNumber));
        booking = hotel.findBookingByConfirmationNumber(confNumber);

        // assert
        assertTrue(booking.isPending());
        assertTrue(room.isReady());
        assertFalse(room.isAvailable(date, stayLength));
    }

    @Test
    void TestCheckinMethod_Hotel_Room_Booking_Integration_CorrectStates() {
        //arrange
        booking = hotel.findBookingByConfirmationNumber(confNumber);
        assertTrue(room.isReady());
        assertTrue(booking.isPending());

        // act
        hotel.checkin(confNumber);

        // assert
        assertTrue(booking.isCheckedIn());
        assertTrue(room.isOccupied());

    }

    @Test
    void TestCheckinMethod_Hotel_Booking_Integration_BookingCheckedIn() {
        //arrange
        booking = hotel.findBookingByConfirmationNumber(confNumber);
        assertTrue(booking.isPending());
        booking.checkIn();

        // act
        Executable e = () -> hotel.checkin(confNumber);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertTrue(booking.isCheckedIn());
        assertEquals("Booking: checkIn : bad state : CHECKED_IN", t.getMessage());
    }

    @Test
    void TestCheckinMethod_Hotel_Room_Booking_Integration_RoomOccupied() {
        //arrange
        assertTrue(room.isReady());
        room.checkin();
        assertTrue(room.isOccupied());
        // act
        Executable e = () -> hotel.checkin(confNumber);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertTrue(room.isOccupied());
        assertEquals("Room: checkin : bad state : OCCUPIED", t.getMessage());
    }

    @Test
    void TestCheckoutMethod_Hotel_Room_Booking_Integration_CorrectStates() {
        //arrange
        hotel.checkin(confNumber);
        Booking booking = hotel.findActiveBookingByRoomId(room.getId());
        assertTrue(booking.isCheckedIn());
        assertTrue(room.isOccupied());

        //act
        hotel.checkout(room.getId());

        //assert
        assertTrue(booking.isCheckedOut());
        assertTrue(room.isReady());
    }

    @Test
    void TestCheckoutMethod_Hotel_Room_Booking_Integration_BookingNotCheckedIn() {
        //arrange
        hotel.checkin(confNumber);
        booking = hotel.findActiveBookingByRoomId(room.getId());
        assertTrue(booking.isCheckedIn());
        booking.checkOut();
        assertTrue(booking.isCheckedOut());

        //act
        Executable e = () -> hotel.checkout(room.getId());
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertTrue(booking.isCheckedOut());
        assertEquals("Booking: checkOut : bad state : CHECKED_OUT", t.getMessage());
    }

    @Test
    void TestCheckoutMethod_Hotel_Room_Booking_Integration_RoomReady() {
        //arrange
        booking = hotel.findBookingByConfirmationNumber(confNumber);
        hotel.checkin(confNumber);
        room.checkout(booking);


        //act
        Executable e = () -> hotel.checkout(room.getId());
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertTrue(room.isReady());
        assertEquals("Room: checkout : bad state : READY", t.getMessage());
    }

    @Test
    void TestAddServiceChargeMethod_Hotel_Booking_Integration_CorrectStates() {
        //arrange
        hotel.checkin(confNumber);
        booking = hotel.findActiveBookingByRoomId(room.getId());
        assertTrue(booking.getCharges().size() == 0);

        // act
        hotel.addServiceCharge(room.getId(), ServiceType.ROOM_SERVICE, 200);

        //verify
        assertTrue(booking.getCharges().size() == 1);
    }

    @Test
    void TestAddServiceChargeMethod_Hotel_Booking_Integration_BookingCheckedOut() {
        //arrange
        hotel.checkin(confNumber);
        booking = hotel.findActiveBookingByRoomId(room.getId());
        assertTrue(booking.getCharges().size() == 0);
        booking.checkOut();

        // act
        Executable e = () -> hotel.addServiceCharge(room.getId(), ServiceType.ROOM_SERVICE, 200);
        Throwable t = assertThrows(RuntimeException.class, e);

        //verify
        assertEquals("Booking: addServiceCharge : bad state : CHECKED_OUT", t.getMessage());
        assertTrue(booking.getCharges().size() == 0);
    }
}