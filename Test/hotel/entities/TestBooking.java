package hotel.entities;

import hotel.credit.CreditCard;
import hotel.helpers.ServiceChargeHelper;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)


class TestBooking {

    //Thank you for Scott for helping me - issues around addServiceCharge not populating anything was due to
    // mocking of room (was just @Mock Room room) and ServiceType (was just ServiceType serviceType).
    // without his assistance and guidance would be still suck.


    @Mock Room mockRoom = new Room(101, RoomType.SINGLE);
    @Mock Guest mockGuest;
    @Mock CreditCard mockCard;
    ServiceType chargeType = ServiceType.ROOM_SERVICE;
    double serviceCost = 20;
    Date date = new GregorianCalendar(2018, 10, 5).getTime();
    int stayLen = 1;
    int numOccupants = 1;

    @InjectMocks Booking booking = new Booking(mockGuest, mockRoom, date, stayLen,numOccupants, mockCard);
    @Spy ArrayList<ServiceCharge> charges;
    @Spy ServiceChargeHelper serviceChargeHelper;


    @BeforeEach
       void setUp() throws RuntimeException{

   }


    @AfterEach
    void tearDown() throws RuntimeException {
    }


    @Test
    void testCheckInIfPending(){
    //arrange
        assertTrue(booking.isPending());

    //act
        booking.checkIn();

    //assert
        assertTrue(booking.isCheckedIn());
        assertFalse(mockRoom.isReady());
    }


    @Test
    void testCheckInException(){
    //arrange
       booking.checkIn();
       assertFalse(booking.isPending());

    //act
        Executable e = () -> booking.checkIn();
        Throwable t = assertThrows(RuntimeException.class, e);

    //assert
        assertEquals("Booking: checkIn : bad state : CHECKED_IN", t.getMessage());
    }


    @Test
    void testCheckOutIfCheckedIn(){
        //arrange
        booking.checkIn();
        assertTrue(booking.isCheckedIn());

        //act
        booking.checkOut();

        //assert
        assertTrue(booking.isCheckedOut());
        verify(mockRoom).checkout(booking);
    }


    @Test
    void testCheckOutException(){
        //arrange
        assertFalse(booking.isCheckedIn());

        //act
        Executable e = () -> booking.checkOut();

        //assert
        Throwable t = assertThrows(RuntimeException.class, e);
        assertEquals("Booking: checkOut : bad state : PENDING", t.getMessage());
    }


    @Test
    void testAddServiceChargeIfCheckedIn(){
        //arrange
        booking.checkIn();

        //act
        booking.addServiceCharge(chargeType, serviceCost);

        //assert
        assertTrue(booking.isCheckedIn());
        assertEquals(1, charges.size());
        //with the below we know that this is called, else the assertEquals wouldn't work - adding this here
        //just to double check and for any large system might be worth while to have.
        verify(serviceChargeHelper).makeServiceCharge(chargeType, serviceCost);
    }


    @Test
    void testAddServiceChargeException(){
        //arrange
        assertFalse(booking.isCheckedIn());

        //act
        Executable e = () -> booking.addServiceCharge(chargeType, serviceCost);
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Booking: addServiceCharge : bad state : PENDING", t.getMessage());
    }
}