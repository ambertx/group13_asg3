package hotel.entities;

import hotel.credit.CreditCard;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class TestHotel {
    int numOccupants;
    int stayLength;
    long mockConfirmationNumber;
    int mockRoomId;
    Date date;
    SimpleDateFormat format;

    @Spy HashMap<Long, Booking> bookingsByConfirmationNumber;
    @Spy HashMap<Integer, Booking> activeBookingsByRoomId;
    @InjectMocks Hotel hotel;


    @Mock Guest guest;
    @Mock CreditCard card;
    @Mock Room room;
    @Mock Booking booking;

    ServiceType serviceType;
    double mockServiceChargeCost;


    @BeforeEach
    void setUp() throws Exception {
        format = new SimpleDateFormat("dd-MM-yyyy");
        date = format.parse("13-10-2018");
        mockConfirmationNumber = 13102018101L;
        mockRoomId = 101;
        numOccupants = 1;
        stayLength = 1;
        mockServiceChargeCost = 100.00;
    }


    @AfterEach
    void tearDown() throws Exception {

    }

    /* This test verifies that the Hotel.book() method returns the booking confirmation number as intended.
     */
    @Test
    void TestBook_ReturnedConfirmationNumber() {
        // arrange
        when(room.book(guest, date, stayLength, numOccupants, card)).thenReturn(booking);
        when(booking.getConfirmationNumber()).thenReturn(mockConfirmationNumber);

        // act
        long returnedValue = hotel.book(room, guest, date, stayLength, numOccupants, card);

        // assert
        verify(room).book(guest, date, stayLength, numOccupants, card);
        assertEquals(mockConfirmationNumber, returnedValue);
    }

    /* This test verifies a new Booking was added to the bookingsByConfirmationNumber map when hotel.book() is called.
     */
    @Test
    void TestBook_BookingExistsInBookingsByConfirmationNumber() {
        // arrange
        when(room.book(guest, date, stayLength, numOccupants, card)).thenReturn(booking);
        when(booking.getConfirmationNumber()).thenReturn(mockConfirmationNumber);

        // act
        long confNumber = hotel.book(room, guest, date, stayLength, numOccupants, card);

        // assert
        verify(bookingsByConfirmationNumber).put(confNumber, booking);
        assertTrue(bookingsByConfirmationNumber.containsKey(confNumber));
    }

    @Test
    void TestCheckin_Exception () {
        // arrange
        // act
        Executable e = () -> hotel.checkin(mockConfirmationNumber);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertEquals("Can not check in. No booking is associated with the specified confirmation number.", t.getMessage());
        assertTrue(activeBookingsByRoomId.size() == 0);
    }

    /* Tests the the effect that booking.checkIn() has on botel.checkin() when it throws a RuntimeException.
       The booking shouldn't be added to the activeBookingsByRoomId Map.
     */
    @Test
    void TestCheckin_BookingCheckInThrowsException() {
        // arrange
        bookingsByConfirmationNumber.put(mockConfirmationNumber, booking);
        doThrow(new RuntimeException("Error")).when(booking).checkIn();
        // act

        Executable e = () -> hotel.checkin(mockConfirmationNumber);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        verify(booking).checkIn();
        assertEquals("Error", t.getMessage());
        assertTrue(activeBookingsByRoomId.size() == 0);
    }


    @Test
    void TestCheckin_BookingExistsInActiveBookingsByRoomId() {
        // arrange
        when(booking.getRoomId()).thenReturn(mockRoomId);
        bookingsByConfirmationNumber.put(mockConfirmationNumber, booking);

        // act
        hotel.checkin(mockConfirmationNumber);

        // assert
        verify(activeBookingsByRoomId).put(mockRoomId, booking);
        assertTrue(activeBookingsByRoomId.containsKey(booking.getRoomId()));
        assertNotNull(hotel.findActiveBookingByRoomId(booking.getRoomId()));
        assertTrue(activeBookingsByRoomId.size() == 1);
    }

    @Test
    void TestCheckout_Exception() {
        // arrange
        // act
        Executable e = () -> hotel.checkout(mockRoomId);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertEquals("Can not check out of a room that doesn't have an active booking.", t.getMessage());
    }

    /* Tests the the effect that booking.checkOut() has on botel.checkout() when it throws a RuntimeException.
       The booking shouldn't be removed from the activeBookingsByRoomId Map.
     */
    @Test
    void TestCheckout_BookingCheckOutThrowsException() {
        // arrange
        activeBookingsByRoomId.put(mockRoomId, booking);
        doThrow(new RuntimeException("Error")).when(booking).checkOut();

        // act
        Executable e = () -> hotel.checkout(mockRoomId);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        verify(booking).checkOut();
        assertEquals("Error", t.getMessage());
        assertTrue(activeBookingsByRoomId.containsKey(mockRoomId));
    }


    @Test
    void TestCheckout_BookingIsRemovedFromActiveBookingsByRoomId() {
        // arrange
        activeBookingsByRoomId.put(mockRoomId, booking);
        assertTrue(activeBookingsByRoomId.size() == 1);
        // act
        hotel.checkout(mockRoomId);

        // assert
        verify(activeBookingsByRoomId).remove(mockRoomId);
        assertTrue(!activeBookingsByRoomId.containsKey(mockRoomId));
    }

    @Test
    void TestAddServiceCharge_ThrowsException() {
        // arrange
        // act
        Executable e = () -> hotel.addServiceCharge(mockRoomId, serviceType, mockServiceChargeCost);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertEquals("Can not add a service charge to a booking that isn't active.", t.getMessage());
    }

    @Test
    void TestAddServiceCharge_BookingAddServiceChargeThrowsException() {
        // arrange
        activeBookingsByRoomId.put(mockRoomId, booking);
        doThrow(new RuntimeException("Error")).when(booking).addServiceCharge(serviceType, mockServiceChargeCost);

        // act
        Executable e = () -> hotel.addServiceCharge(mockRoomId, serviceType, mockServiceChargeCost);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        verify(booking).addServiceCharge(serviceType, mockServiceChargeCost);
        assertEquals("Error", t.getMessage());
    }

    @Test
    void TestAddServiceCharge_ValidOperation() {
        // arrange
        activeBookingsByRoomId.put(mockRoomId, booking);
        // If the addServiceCharge() method in Booking were to use a factory class for the creation of new ServiceCharge
        // objects we could check whether the method creating the ServiceCharge objects is called.
        // Since the implementation creates a ServiceType object via the new keyword, there's not really a lot we can do to
        // verify the ServiceCharge object has actually been created.

        // With that said, we can check whether an exception has been thrown. If no exception has been thrown we can be pretty
        // confident the new ServiceCharge object has been added to the specified booking.

        // act
        Executable e = () -> hotel.addServiceCharge(mockRoomId, serviceType, mockServiceChargeCost);

        // assert
        assertDoesNotThrow(e);
        verify(booking).addServiceCharge(serviceType, mockServiceChargeCost);
    }
}