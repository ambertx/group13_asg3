package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)

class RoomTest {

    @Mock Booking booking;
    @Spy ArrayList<Booking> bookings;

    int roomID = 5;
    RoomType roomType = RoomType.DOUBLE;

    @InjectMocks Room room = new Room(roomID, roomType);

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    @BeforeEach
    void setUp() throws RuntimeException{
    }

    @AfterEach
    void tearDown() throws RuntimeException{
    }

    @Test
    void testCheckInWhenOccupied() {
        //arrange
        room.state = Room.State.OCCUPIED;
        //act
        Executable e = () -> room.checkin();
        Throwable t = assertThrows(RuntimeException.class, e);
        //assert
        assertEquals("Room: checkin : bad state : OCCUPIED", t.getMessage());
    }

    @Test
    void testCheckinWhenREADY() {
        //arrange
        //act
        room.checkin();
        //assert
        assertTrue(room.isOccupied());
    }

    @Test
    void testCheckinWhenOccupied() {
        //arrange
        room.checkin();
        //act
        Executable e = () -> room.checkin();
        Throwable t = assertThrows(RuntimeException.class, e);
        //assert
        assertEquals("Room: checkin : bad state : OCCUPIED", t.getMessage());
    }

    @Test
    void testCheckoutWhenOccupied() {
        //arrange
        bookings.add(booking);
        room.checkin();
        assertEquals(1, bookings.size());
        assertTrue(room.isOccupied());
        //act
        room.checkout(booking);
        //assert
        verify(bookings).remove(booking);
        assertTrue(room.isReady());
        assertEquals(0, bookings.size());
    }

    @Test
    void testCheckoutWhenNotOccupied() {
        //arrange
        assertEquals(0, bookings.size());
        assertFalse(room.isOccupied());
        //act
        Executable e = () -> room.checkout(booking);
        Throwable t = assertThrows(RuntimeException.class, e);
        //assert
        assertEquals("Room: checkout : bad state : READY", t.getMessage());
    }

    @Test
    void testCheckoutRemovesBooking() {
        //arrange
        bookings.add(booking);
        room.checkin();
        assertEquals(1, bookings.size());
        //act
        room.checkout(booking);
        //assert
        assertEquals(0, bookings.size());
    }

    @Test
    void testBookInsertsIntoBookings() {
        //arrange
        assertEquals(0, bookings.size());
        //act
        bookings.add(booking);
        //assert
        assertEquals(1, bookings.size());
    }
}